import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { MaterialModule } from "../material.module";

const routes: Routes = [
    {path:'', component:LoginComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes),MaterialModule],
  declarations:[LoginComponent],
  exports: [RouterModule]
})
export class UserManagementRoutingModule { }

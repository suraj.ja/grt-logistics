import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder,Validators, FormControl, FormGroupDirective, NgForm } from "@angular/forms";
import { ErrorStateMatcher } from '@angular/material/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm:FormGroup;hide:Boolean = true;
  constructor(private fb:FormBuilder) { }

  ngOnInit(): void {
    this.loginFormInit()
  }
  loginFormInit(){
    this.loginForm = this.fb.group({
      email:[null, Validators.required],
      password:[null, Validators.required],
    })
  }
  submit(e,val){
    console.log(e)
    e.preventDefault();
    e.stopPropagation();
    return false;
  }
}

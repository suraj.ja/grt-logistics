import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule} from "@angular/common";
const routes: Routes = [
  {path:'login', loadChildren: () => import('./user-management/user-management-router.module').then(m => m.UserManagementRoutingModule)},
  {path:'', redirectTo:'login', pathMatch: 'full'},
  { path: '**', redirectTo:'login', pathMatch: 'full'},
]
@NgModule({
  imports: [RouterModule.forRoot(routes,{scrollPositionRestoration: 'enabled'}),CommonModule],
  exports: [RouterModule],
  // providers: [{provide: LocationStrategy, useClass: HashLocationStrategy}],
})
export class AppRoutingModule { }
